# Concepts-api

This service holds all the data for the `concepts` project.

## Quick start

//TODO

## Migrations

```sh
export HASURA_ENDPOINT='http://localhost:8080'
export HASURA_SECRET='password'
yarn h-status
yarn h-migrate
yarn h-meta
```

## Scripts

### dump schemas\data

```sh
chmod +x ./scripts/*.sh
export HASURA_ENDPOINT='http://localhost:8080'
export HASURA_SECRET='password'
./scripts/pg_dump_schemas.sh > schemas.sql
./scripts/pg_dump_data.sh > data.sql
```

### migrate log

You'll need to load the `logs-migration-flow.json` to a [node-red](https://nodered.org/) instance.
Then you can provide the relevant parameters in the request node and run the flow.
