curl -X POST \
     -H "Content-Type: application/json" \
     -H "x-hasura-admin-secret: ${HASURA_SECRET}" \
     -d '{"opts": ["-O", "-x", "--inserts", "--data-only"], "clean_output": true}' \
     ${HASURA_ENDPOINT}/v1alpha1/pg_dump