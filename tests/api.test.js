import { query } from "graphqurl";
import { buildReq } from "./gql-client";

const logActivities = `
mutation logActivities($activities: [logs_activities_insert_input!]!) {
  insert_logs_activities(objects: $activities) {
    affected_rows
  }
}
`;

const fetchLog = `
{
  logs_activities(limit: 10) {
    id
    activity
    start_time
    duration
    device
    payload
  }
}
`;

const fetchConcepts = `
{
  concepts(limit: 10) {
    created_at
    id
    name
    children {
      created_at
      description
      id
      updated_at
      child {
        created_at
        id
        name
        updated_at
      }
    }
    parents {
      description
      id
      parent {
        created_at
        id
        name
        updated_at
      }
      updated_at
      created_at
    }
    updated_at
    resources_tags {
      resource {
        created_at
        id
        url
      }
    }
  }
}
`;

test("fetch concepts", () =>
  query(
    buildReq({
      query: fetchConcepts,
    })
  ));

test("fetch log", () =>
  query(
    buildReq({
      query: fetchLog,
    })
  ));

test("log activities", () =>
  query(
    buildReq({
      query: logActivities,
      variables: {
        activities: [
          {
            device: "laptop",
            activity: "chrome",
            payload: { url: "https://www.google.com" },
            start_time: new Date().toUTCString(),
            duration: "1000",
          },
          {
            device: "phone",
            activity: "firefox",
            payload: { url: "https://gitlab.com" },
            start_time: new Date().toUTCString(),
            duration: "500",
          },
        ],
      },
    })
  ));
