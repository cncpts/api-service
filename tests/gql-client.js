import { merge } from "ramda";
require("dotenv").config();

export const client = {
  endpoint: `${process.env.HASURA_ENDPOINT}/v1/graphql`,
  headers: {
    "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET,
  },
};

export const buildReq = (req) => merge(client, req);
