DROP TRIGGER IF EXISTS "set_concepts_resource_concept_tag_updated_at" ON "concepts"."resource_concept_tag";
ALTER TABLE "concepts"."resource_concept_tag" DROP COLUMN "updated_at";
