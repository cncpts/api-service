ALTER TABLE "concepts"."resource_concept_tag" ADD COLUMN "updated_at" timestamptz NULL DEFAULT now();

CREATE OR REPLACE FUNCTION "concepts"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_concepts_resource_concept_tag_updated_at"
BEFORE UPDATE ON "concepts"."resource_concept_tag"
FOR EACH ROW
EXECUTE PROCEDURE "concepts"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_concepts_resource_concept_tag_updated_at" ON "concepts"."resource_concept_tag" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
