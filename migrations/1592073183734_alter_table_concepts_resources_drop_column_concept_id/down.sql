ALTER TABLE "concepts"."resources" ADD COLUMN "concept_id" int4;
ALTER TABLE "concepts"."resources" ALTER COLUMN "concept_id" DROP NOT NULL;
ALTER TABLE "concepts"."resources" ADD CONSTRAINT references_node_id_fkey FOREIGN KEY (concept_id) REFERENCES "concepts"."concepts" (id) ON DELETE cascade ON UPDATE cascade;
