
ALTER TABLE "data"."entries" ALTER COLUMN "end_time" TYPE timestamptz;
ALTER TABLE "data"."entries" ADD CONSTRAINT "entries_end_time_key" UNIQUE ("end_time")
COMMENT ON COLUMN "data"."entries"."end_time" IS ''