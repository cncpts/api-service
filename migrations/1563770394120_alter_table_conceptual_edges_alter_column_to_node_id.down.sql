
ALTER TABLE "conceptual"."edges" ALTER COLUMN "to_node_id" TYPE integer;
COMMENT ON COLUMN "conceptual"."edges"."to_node_id" IS 'null'
alter table "conceptual"."edges" rename column "parent_node_id" to "to_node_id";