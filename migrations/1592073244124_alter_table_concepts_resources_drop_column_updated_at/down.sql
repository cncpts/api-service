ALTER TABLE "concepts"."resources" ADD COLUMN "updated_at" timestamptz;
ALTER TABLE "concepts"."resources" ALTER COLUMN "updated_at" DROP NOT NULL;
ALTER TABLE "concepts"."resources" ALTER COLUMN "updated_at" SET DEFAULT now();
