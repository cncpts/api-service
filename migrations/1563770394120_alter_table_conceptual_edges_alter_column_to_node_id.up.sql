
ALTER TABLE "conceptual"."edges" ALTER COLUMN "to_node_id" TYPE int4;
COMMENT ON COLUMN "conceptual"."edges"."to_node_id" IS ''
alter table "conceptual"."edges" rename column "to_node_id" to "parent_node_id";