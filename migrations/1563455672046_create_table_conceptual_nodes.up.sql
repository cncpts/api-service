
CREATE TABLE "conceptual"."nodes"("id" serial NOT NULL, "name" text NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") );
CREATE OR REPLACE FUNCTION "conceptual"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_conceptual_nodes_updated_at"
BEFORE UPDATE ON "conceptual"."nodes"
FOR EACH ROW
EXECUTE PROCEDURE "conceptual"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_conceptual_nodes_updated_at" ON "conceptual"."nodes" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
