
CREATE TABLE "conceptual"."references"("id" serial NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "url" text NOT NULL, "node_id" integer NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("node_id") REFERENCES "conceptual"."nodes"("id") ON UPDATE cascade ON DELETE cascade);
CREATE OR REPLACE FUNCTION "conceptual"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_conceptual_references_updated_at"
BEFORE UPDATE ON "conceptual"."references"
FOR EACH ROW
EXECUTE PROCEDURE "conceptual"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_conceptual_references_updated_at" ON "conceptual"."references" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
