
ALTER TABLE "conceptual"."edges" ALTER COLUMN "from_node_id" TYPE int4;
COMMENT ON COLUMN "conceptual"."edges"."from_node_id" IS ''
alter table "conceptual"."edges" rename column "from_node_id" to "child_node_id";