
create or REPLACE view pending_process.resources_to_tag as
with resources as
(SELECT min(id) as id, payload->>'url' as uri, sum(duration) as duration, max(start_time) as last_visted
FROM logs.activities
where activity = 'web browsing'
group by uri)
select resources.* from resources
left join pending_process.ignored_resources on resources.uri ilike ignored_resources.uri_regex
where ignored_resources.id is null;