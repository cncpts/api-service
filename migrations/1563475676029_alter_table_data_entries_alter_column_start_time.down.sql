
ALTER TABLE "data"."entries" ALTER COLUMN "start_time" TYPE timestamp with time zone;
ALTER TABLE "data"."entries" DROP CONSTRAINT "entries_start_time_key"
COMMENT ON COLUMN "data"."entries"."start_time" IS 'null'