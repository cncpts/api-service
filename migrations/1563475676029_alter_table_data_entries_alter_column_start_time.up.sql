
ALTER TABLE "data"."entries" ALTER COLUMN "start_time" TYPE timestamptz;
ALTER TABLE "data"."entries" ADD CONSTRAINT "entries_start_time_key" UNIQUE ("start_time")
COMMENT ON COLUMN "data"."entries"."start_time" IS ''