
ALTER TABLE "concepts"."relations" ALTER COLUMN "parent_node_id" TYPE integer;
COMMENT ON COLUMN "concepts"."relations"."parent_node_id" IS E'null'
alter table "concepts"."relations" rename column "parent_concept_id" to "parent_node_id";