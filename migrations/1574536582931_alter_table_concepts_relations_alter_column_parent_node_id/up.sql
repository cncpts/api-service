
ALTER TABLE "concepts"."relations" ALTER COLUMN "parent_node_id" TYPE int4;
COMMENT ON COLUMN "concepts"."relations"."parent_node_id" IS E''
alter table "concepts"."relations" rename column "parent_node_id" to "parent_concept_id";