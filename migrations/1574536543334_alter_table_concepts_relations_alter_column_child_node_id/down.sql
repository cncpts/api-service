
ALTER TABLE "concepts"."relations" ALTER COLUMN "child_node_id" TYPE integer;
COMMENT ON COLUMN "concepts"."relations"."child_node_id" IS E'null'
alter table "concepts"."relations" rename column "child_concept_id" to "child_node_id";