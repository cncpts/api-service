
ALTER TABLE "concepts"."relations" ALTER COLUMN "child_node_id" TYPE int4;
COMMENT ON COLUMN "concepts"."relations"."child_node_id" IS E''
alter table "concepts"."relations" rename column "child_node_id" to "child_concept_id";