
ALTER TABLE "logs"."activities" ADD COLUMN "end_time" timestamptz
ALTER TABLE "logs"."activities" ALTER COLUMN "end_time" DROP NOT NULL