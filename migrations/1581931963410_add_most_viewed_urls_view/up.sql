
create or REPLACE view logs.most_viewed_urls as
SELECT min(id) as id, payload->'url' as url, sum(duration) as duration, max(start_time) as last_visted
FROM logs.activities
where activity = 'web browsing'
group by url
order by duration desc;