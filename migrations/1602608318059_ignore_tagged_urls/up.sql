CREATE OR REPLACE VIEW "pending_process"."resources_to_tag" AS 
 WITH resources AS (
         SELECT min(activities.id) AS id,
            (activities.payload ->> 'url'::text) AS uri,
            sum(activities.duration) AS duration,
            max(activities.start_time) AS last_visted
           FROM logs.activities
          WHERE (activities.activity = 'web browsing'::text)
          GROUP BY (activities.payload ->> 'url'::text)
        )
 SELECT resources.id,
    resources.uri,
    resources.duration,
    resources.last_visted
   FROM resources
     LEFT JOIN pending_process.ignored_resources ON resources.uri ~~* ignored_resources.uri_regex
	 LEFT JOIN concepts.resources AS concepts_resources ON resources.uri = concepts_resources.url
  WHERE (ignored_resources.id IS NULL AND concepts_resources.id IS NULL);
