
ALTER TABLE "conceptual"."edges" ALTER COLUMN "from_node_id" TYPE integer;
COMMENT ON COLUMN "conceptual"."edges"."from_node_id" IS 'null'
alter table "conceptual"."edges" rename column "child_node_id" to "from_node_id";