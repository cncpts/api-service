CREATE TRIGGER "set_conceptual_references_updated_at"
BEFORE UPDATE ON "concepts"."resources"
FOR EACH ROW EXECUTE FUNCTION concepts.set_current_timestamp_updated_at();COMMENT ON TRIGGER "set_conceptual_references_updated_at" ON "concepts"."resources"
IS E'trigger to set value of column "updated_at" to current timestamp on row update';
