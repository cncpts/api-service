
ALTER TABLE "concepts"."references" ALTER COLUMN "node_id" TYPE integer;
COMMENT ON COLUMN "concepts"."references"."node_id" IS E'null'
alter table "concepts"."references" rename column "concept_id" to "node_id";