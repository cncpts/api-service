
ALTER TABLE "concepts"."references" ALTER COLUMN "node_id" TYPE int4;
COMMENT ON COLUMN "concepts"."references"."node_id" IS E''
alter table "concepts"."references" rename column "node_id" to "concept_id";