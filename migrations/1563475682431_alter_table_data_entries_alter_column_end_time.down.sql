
ALTER TABLE "data"."entries" ALTER COLUMN "end_time" TYPE timestamp with time zone;
ALTER TABLE "data"."entries" DROP CONSTRAINT "entries_end_time_key"
COMMENT ON COLUMN "data"."entries"."end_time" IS 'null'